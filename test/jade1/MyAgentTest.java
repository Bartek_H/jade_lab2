/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jade1;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BBB
 */
public class MyAgentTest {
    
    public MyAgentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of computeMatrix method, of class MyAgent.
     */
    @Test
    public void testComputeMatrix1() {
        System.out.println("computeMatrix1");
        String value = "1$1$1$1@1$1$1$1";
        MyAgent instance = new MyAgent();
        String expResult = "4";
        String result;
        try {
            result = instance.computeMatrix(value);
            assertEquals(expResult, result);
        } catch (Exception ex) {
            Logger.getLogger(MyAgentTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }   
    
    /**
     * Test of computeMatrix method, of class MyAgent.
     */
    @Test
    public void testComputeMatrix2() {
        System.out.println("computeMatrix2");
        String value = "-1$1$1$1@1$1$1$1";
        MyAgent instance = new MyAgent();
        String expResult = "2";
        String result;
        try {
            result = instance.computeMatrix(value);
            assertEquals(expResult, result);
        } catch (Exception ex) {
            Logger.getLogger(MyAgentTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
        /**
     * Test of computeMatrix method, of class MyAgent.
     */
    @Test
    public void testComputeMatrix3() {
        System.out.println("computeMatrix3");
        String value = "-1$1$1$1@-1$1$1$1";
        MyAgent instance = new MyAgent();
        String expResult = "4";
        String result;
        try {
            result = instance.computeMatrix(value);
            assertEquals(expResult, result);
        } catch (Exception ex) {
            Logger.getLogger(MyAgentTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
        /**
     * Test of computeMatrix method, of class MyAgent.
     */
    @Test
    public void testComputeMatrix4() {
        System.out.println("computeMatrix4");
        String value = "-1$1$1$5@-1$1$1$1";
        MyAgent instance = new MyAgent();
        String expResult = "8";
        String result;
        try {
            result = instance.computeMatrix(value);
            assertEquals(expResult, result);
        } catch (Exception ex) {
            Logger.getLogger(MyAgentTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * Test of computeMatrix method, of class MyAgent.
     */
    @Test
    public void testComputeMatrix5() {
        System.out.println("computeMatrix5");
        String value = "-100000$1520$21321$3335@1123$132$13232$32321";
        MyAgent instance = new MyAgent();
        String expResult = "277810647";
        String result;
        try {
            result = instance.computeMatrix(value);
            assertEquals(expResult, result);
        } catch (Exception ex) {
            Logger.getLogger(MyAgentTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }    
}
