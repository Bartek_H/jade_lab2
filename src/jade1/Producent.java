/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jade1;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Bartosz Hajdaś
 */
public class Producent extends Agent 
{
    HashMap<String, MyToken> tokenList = new HashMap<String, MyToken>();
    int tokenNumber = 0;
    int matrixSize = 100;
    ArrayList<AID> agentList = new ArrayList();
    long startTime = System.nanoTime();
    long endTime = 0;    
    
    MyMatrix matrixA = null;
    MyMatrix matrixB = null;
    MyMatrix matrixC = null;
	
    protected void setup() 
    {
        Object[] args = getArguments();
        
        if((args != null) && (2 <= args.length))
        {
            int agentCount = Integer.parseInt((String) args[0]);
            matrixSize = Integer.parseInt((String) args[1]);
            if(0 >= matrixSize)
            {
                throw  new IndexOutOfBoundsException("Wrong matrix size");
            }           
            
            matrixA = new MyMatrix(matrixSize);
            matrixA.generateMatrix();
            //matrixA.wypisz();
            matrixA.saveToFile("matrixA.txt");
            
            matrixB = new MyMatrix(matrixSize);
            matrixB.generateMatrix();
            //matrixB.wypisz();
            matrixB.saveToFile("matrixB.txt");
            
            matrixC = new MyMatrix(matrixSize);
            matrixC.zeros();
            prepareDataToCompute();

            AgentContainer c = getContainerController();
            try 
            {
                for(int i = 0; i < agentCount; i++)
                {
                    String agentName = "Agent" + i;
                    AgentController a = c.createNewAgent(agentName, "jade1.MyAgent", prepareData(agentName,getAID()));
                    a.start();    
                    agentList.add(new AID(agentName, false));
                    MyLogger.log(this, "+++ Created: Agent" + i, MyLogger.PRIORITY.LOW);
                }
            } 
            catch (Exception e) 
            {
                //nothing to do
            }            
        }
        
        addBehaviour(new SimpleBehaviour(this) {
            @Override
            public void action() {
                ACLMessage msg = receive();
                if(null != msg)
                {
                    MyLogger.log(myAgent, "Otrzymalem wiadomosc od " + msg.getSender().getLocalName()+ " List size = "+ tokenList.size() , MyLogger.PRIORITY.LOW);
                    ACLMessage reply = msg.createReply();
                    
                    switch(msg.getPerformative())
                    {
                        case ACLMessage.REQUEST:
                            String tokenId = Integer.toString(tokenNumber);
                            MyToken tokenToDo = tokenList.get(tokenId);
                            if(null != tokenToDo)
                            {
                                tokenToDo.setCurrentState(MyToken.STATES.COMPUTE);
                                reply.setPerformative(ACLMessage.CONFIRM);
                                reply.setConversationId(tokenId);
                                reply.setContent(tokenToDo.getValue());
                                tokenNumber++;
                                send(reply);
                            }
                            break;
                            
                        case ACLMessage.AGREE:
                            int value = Integer.valueOf(msg.getContent());
                            MyToken ob = tokenList.get(msg.getConversationId());
                            
                            if(isInRange(ob))
                            {                                
                                matrixC.getMatrix()[ob.getRowPosition()][ob.getColumnPosition()] = value;                            
                            }
                            tokenList.remove(msg.getConversationId());
                            break;
                    }
                }
            }

            @Override
            public boolean done() {
                boolean retVal = tokenList.isEmpty();
                if(true == retVal)
                {
                    ACLMessage msg = new ACLMessage(ACLMessage.DISCONFIRM);
                    for(int i = 0; i< agentList.size(); i++)
                    {
                        msg.addReceiver(agentList.get(i));
                    }
                    MyLogger.log(myAgent, "WYSYLAM WYLACZENIE", MyLogger.PRIORITY.LOW);
                    send(msg);
                    
                    endTime = System.nanoTime();                    
                    MyLogger.log(myAgent, "System pracował :" + (endTime - startTime)/1000000000 + " sekund" , MyLogger.PRIORITY.HIGH); 
                    //matrixC.wypisz();
                    matrixC.saveToFile("matrixC.txt");
                    doDelete();                   
                }
                return retVal;
            }
        });
    }
    
    private boolean isInRange(MyToken ob)
    {        
        boolean isInRange = false;
        if((ob.getRowPosition() < matrixSize) && (ob.getColumnPosition() < matrixSize))
        {
            isInRange = true;
        }
        return isInRange;
    }
    
    private Object [] prepareData(String agentName, AID parent)
    {
        int iTime = (StaticRandom.getNextUInt() % 100) + 200;        
        Object [] dataList = new Object[2];
        dataList[0] = iTime;
        dataList[1] = parent;        
        MyLogger.log(this, "Dane dla agenta: " + agentName + " Czas reakcji :" + iTime, MyLogger.PRIORITY.HIGH);
        
        return dataList;
    }
    
    private void prepareDataToCompute()
    {
        int row = 0, column =0;
        for(int i = 0; i < Math.pow(matrixSize, 2); i++)
        {
            if(0 == (column % matrixSize) && (0 != column))
            {
                column = 0;
                row++;
            }
            String tokenString = matrixA.getRowInString(row) + "@" + matrixB.getColumnInString(column);
            tokenList.put(Integer.toString(i), new MyToken(tokenString,row,column)); 
            column++;
        }    
    }
}