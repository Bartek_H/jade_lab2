/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jade1;

import jade.core.Agent;

/**
 *
 * @author Bartosz Hajdaś
 */
public class MyLogger {
    
    /**
     * loggerPriority - set LOW/HIGH to display LOW-HIGH/HIGH priority logs.
     */
    static PRIORITY loggerPriority = PRIORITY.LOW;
    
    enum PRIORITY{LOW, HIGH};
    
    static void log(Agent agent,String log, PRIORITY prio)
    {
        if(loggerPriority.ordinal() <= prio.ordinal())
        {
            System.out.println("Agent:" + agent.getName() + " *** " + log);
        }
   }     
}
