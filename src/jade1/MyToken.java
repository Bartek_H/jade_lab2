/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jade1;

/**
 *
 * @author Bartosz Hajdaś
 */
public class MyToken {

    public MyToken(String value, int rowPosition, int columnPosition) {
        this.value = value;
        this.currentState = STATES.INIT;
        this.rowPosition = rowPosition;
        this.columnPosition = columnPosition;
    }

    public String getValue() {
        return value;
    }

    public STATES getCurrentState() {
        return currentState;
    }

    public int getRowPosition() {
        return rowPosition;
    }

    public int getColumnPosition() {
        return columnPosition;
    }

    public void setCurrentState(STATES currentState) {
        this.currentState = currentState;
    }
     
    
    
    
    
    private String value;
    private STATES currentState;
    private int rowPosition;
    private int columnPosition;
    public enum STATES {INIT, COMPUTE, FAILED, CHECK, COMPLETED};  
}
