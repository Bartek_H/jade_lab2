/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jade1;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bartosz Hajdaś
*/
public class MyAgent extends Agent 
{
    public enum STATES {SEND, DO_WORK, ANSWER, DODELETE, SUSPEND};
    private int m_iTime = 0;
    private AID senderID = null;
    STATES state = STATES.SEND;
    ArrayList<String> myTokenList = new ArrayList();    
    
    public String computeMatrix(String value) throws Exception
    {
        String[] sides = value.split("@");
        String retVal = "";
        if(2 == sides.length)
        {
            int result = 0;
            String[] tabStrA = sides[0].split("\\$");
            String[] tabStrB = sides[1].split("\\$");
            int[] tabIntA = Arrays.stream(tabStrA).mapToInt(Integer::parseInt).toArray();
            int[] tabIntB = Arrays.stream(tabStrB).mapToInt(Integer::parseInt).toArray();
            for(int i = 0; i < tabIntA.length; i++)
            {
                result += tabIntA[i]*tabIntB[i];
            }
            retVal = Integer.toString(result);
        }
        else
        {
            throw new Exception("Wrong situation in computeMatrix value = " + value);
        }
        return retVal;
    }
    
    protected void setup() 
    {
        MyLogger.log(this, "Wstałem : "+ getLocalName(), MyLogger.PRIORITY.LOW);
        
        Object[] args = getArguments();
        
        if(null != args)
        {
            if(2 <= args.length )
            {
                try
                {
                    m_iTime = (int)args[0];
                    senderID = (AID)args[1];
                }
                catch (Exception ex)
                {
                    doDelete();
                }                
            }
            else
            {
                doDelete();
                throw new NullPointerException("Wrong arguments!!!");            
            }
        }
        else
        {
            doDelete();
            throw new NullPointerException("Wrong arguments!!!");
        }
        
        addBehaviour(new TickerBehaviour(this, m_iTime) 
        {
            @Override
            protected void onTick() 
            {
                switch(state)
                {
                    case SEND:
                        state = STATES.DO_WORK;
                        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                        msg.setConversationId("");
                        msg.addReceiver(senderID);
                        send(msg);                        
                        break;
                    case DO_WORK: 
                        ACLMessage answer = receive();
                        if(null != answer)
                        {
                            if(ACLMessage.CONFIRM == answer.getPerformative())
                            {
                                MyLogger.log(myAgent, "Otrzymalem zadanie : "+ answer.getPerformative(), MyLogger.PRIORITY.LOW);
                                ACLMessage reply = answer.createReply();
                                state = STATES.SUSPEND;
                                try 
                                {
                                    reply.setPerformative(ACLMessage.AGREE);
                                    reply.setContent(computeMatrix(answer.getContent()));
                                } 
                                catch (Exception ex) 
                                {
                                    reply.setContent("");
                                    Logger.getLogger(MyAgent.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                send(reply);                                
                            }
                            else if(ACLMessage.DISCONFIRM == answer.getPerformative())
                            {
                                MyLogger.log(myAgent, "Otrzymałem polecenie wyłączenia : " + answer.getPerformative(), MyLogger.PRIORITY.LOW);
                                state = STATES.DODELETE;
                                
//                                for(int i = 0; i < myTokenList.size(); i++)
//                                {
//                                    MyLogger.log(myAgent, "Przetworzyłem token: " + myTokenList.get(i), MyLogger.PRIORITY.LOW);                                
//                                }
//                                MyLogger.log(myAgent, "Przetworzyłem : " + myTokenList.size()+ " tokenow", MyLogger.PRIORITY.HIGH);  
                            }                            
                        }
                        else
                        {
                            MyLogger.log(myAgent, "Nie otrzymałem potwierdzenia", MyLogger.PRIORITY.LOW);
                            state = STATES.SEND;
                        }
                        break;
                        
                    case DODELETE:
                        doDelete();
                        break;
                        
                    case SUSPEND:
                        MyLogger.log(myAgent, "Pauzuje 1 kolejke : ", MyLogger.PRIORITY.LOW);
                        state = STATES.SEND;
                        break;                    
                }
            }
        });
    }
}