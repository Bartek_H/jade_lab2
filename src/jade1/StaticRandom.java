/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jade1;

import static java.lang.Math.abs;
import java.util.Random;

/**
 *
 * @author Bartosz Hajdaś
 */
public class StaticRandom {
    
    private static Random myRandomOb = new Random();
    
    /**
     * Method return non-negavite integer.
     * @return random positive integer.
     */
    public static Integer getNextUInt()
    {
        return abs(myRandomOb.nextInt());
    }
    
    /**
     * Method return random integer.
     * @return random integer.
     */
    public static Integer getNextInt()
    {
        return myRandomOb.nextInt() % 1000;
    }
}
